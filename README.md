# Mailpipe-GUI

This is an implementation of a GUI for the Mailpipe-API (you can write your own, check the swagger docs. found under the mailpipe project to see all the endpoints).

## Login Page

![alt the login page](cypress/screenshots/page_load.cy.js/login.load.png 'The login page')

## The Inbox

![alt the inbox page](cypress/screenshots/success.cy.js/login.authenticate.valid.png 'The inbox page')

## Developers

```bash
dnf install nodejs vips-devel
npm install node-gyp
npm i
```

### Docker / docker-compose

```bash
docker network create --attachable --subnet 172.16.242.0/24 mailpipe
docker-compose -f <docker-compose-file> <build/up/down>
```

## Cypress

```bash
CYPRESS_baseUrl='https://<ip>:<port>' docker-compose -f ./docker-compose-e2e.yml up
```
