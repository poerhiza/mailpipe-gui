/* eslint-disable no-undef */

const defaultCredentials = {
  username: 'mailpipe@changeme.tld',
  password: 'mailpipe@changeme.tld',
};

Cypress.Commands.add(
  'login',
  (
    username,
    password,
    {
      cacheSession = true,
    } = {},
  ) => {
    const login = () => {
      cy.visit('/#!/login');

      cy.title().should('eq', 'Mailpipe');

      cy.get('input[id="login.username"]').type(username);
      cy.get('input[id="login.password"]').type(password);

      cy.get('button[id="login.submit"]').click();

      cy.wait(2000);
    };

    if (cacheSession) {
      cy.session(
        [
          'authenticate',
          username,
          password,
        ],
        login,
      );
    } else {
      login();
    }
  },
);

module.exports = defaultCredentials;
