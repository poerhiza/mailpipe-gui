/* eslint-disable no-undef */

Cypress.session.clearAllSavedSessions();

describe('page load', () => {
  it('should load the login page', () => {
    cy.visit('/#!/login');

    cy.title().should('eq', 'Mailpipe');

    cy.get('button[id="login.submit"]').should('exist');

    cy.wait(1000);

    cy.screenshot('login.load');
  });
});
