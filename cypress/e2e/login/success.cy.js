/* eslint-disable no-undef */

import defaultCredentials from './login';

Cypress.session.clearAllSavedSessions();

describe('success', () => {
  it('should successfully authenticate with valid credentials', () => {
    cy.login(defaultCredentials.username, defaultCredentials.password, { cacheSession: true });

    cy.visit('#!/boxes');

    cy.url().should('include', '/#!/boxes');

    cy.get('a[href="#!/boxes"]').should('exist');
    cy.get('a[href="#!/links"]').should('exist');
    cy.get('a[href="#!/send/new/0/0/0"]').should('exist');
    cy.get('a[href="#!/transports"]').should('exist');

    cy.wait(1000);

    cy.screenshot('login.authenticate.valid');
  });

  it('should logout when requested', () => {
    cy.login(defaultCredentials.username, defaultCredentials.password, { cacheSession: true });

    cy.visit('#!/boxes');

    cy.get('button[id="toolbar.logout"]').click();

    cy.get('button[id="login.submit"]').should('exist');

    cy.url().should('include', '/#!/login');

    cy.wait(1000);

    cy.screenshot('login.authenticate.valid.logout');
  });
});
