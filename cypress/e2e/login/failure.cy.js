/* eslint-disable no-undef */

require('./login');

Cypress.session.clearAllSavedSessions();

describe('failure', () => {
  it('should fail to authenticate with invalid credentials', () => {
    cy.login('notTheDefaultUsername', 'notTheDefaultPassword', { cacheSession: false });

    cy.get('button[id="login.submit"]').should('exist');
    cy.url().should('include', '/#!/login');

    cy.screenshot('login.authenticate.invalid');
  });
});
