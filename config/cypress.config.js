const { defineConfig } = require('cypress');

module.exports = defineConfig({
  video: false,
  e2e: {
    experimentalSessionAndOrigin: true,
    baseUrl: 'https://172.16.242.101:8080/',
    supportFile: false,
    specPattern: 'cypress/e2e/**/*.cy.js',
    viewportWidth: 1280,
    viewportHeight: 720,
  },
});
