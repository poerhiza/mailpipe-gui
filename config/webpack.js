const path = require('path');

const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const Visualizer = require('webpack-visualizer-plugin2');//eslint-disable-line
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const webpack = require('webpack');

const production = JSON.parse(process.env.NODE_ENV === 'production' || '0');

const devServerConfig = {
  host: '0.0.0.0',
  allowedHosts: 'all',
  server: {
    type: 'https',
    options: {
      ca: './config/authority.pem',
      key: './config/private.pem',
      cert: './config/public.pem',
      requestCert: false,
    },
  },
  hot: true,
  historyApiFallback: false,
  compress: true,
  port: 8080,
  open: '/',
  static: {
    directory: path.resolve(__dirname, 'dist'),
  },
  client: {
    progress: true,
    overlay: true,
  },
};

const plugins = [
  new ESLintPlugin({

  }),
  new HtmlWebpackPlugin({
    inject: true,
    template: path.resolve(__dirname, '../src/views/index.ejs'),
    hash: false,
    minify: true,
    filename: 'index.html',
    publicPath: '/',
  }),
  new MiniCssExtractPlugin({
    filename: '[name].css',
  }),
];

let WP_API_HOST = '';

if (production) {
  plugins.push(new CleanWebpackPlugin());
} else {
  plugins.push(new BundleAnalyzerPlugin({
    analyzerHost: '172.16.242.101',
    analyzerPort: '4321',
    openAnalyzer: false,
  }));
  WP_API_HOST = 'https://172.16.242.10:8443';
  devServerConfig.host = '172.16.242.101';
}

plugins.push(new webpack.DefinePlugin({
  SERVICE_URL: JSON.stringify(WP_API_HOST),
}));

plugins.push(new Visualizer({
  filename: path.join('..', 'docs', 'statistics.html'),
}));

module.exports = {
  entry: { app: './src/js/app.js' },
  resolve: {
    fallback: {
      crypto: false,
    },
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: '[name].js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
            plugins: ['angularjs-annotate'],
          },
        },
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
        options: {
          esModule: false,
        },
      },
      {
        test: [/\.css$/],
        use: [
          'style-loader',
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: '/',
              esModule: false,
            },
          },
          'css-loader',
        ],
      },
      {
        test: /\.(ttf|eot|svg|woff2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: 'url-loader?limit=10000',
      },
      {
        test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
        use: 'file-loader',
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          'file-loader?name=images/[name].[ext]',
          'image-webpack-loader?bypassOnDebug',
        ],
      },
    ],
  },
  plugins,
  devServer: devServerConfig,
  devtool: production ? 'hidden-nosources-source-map' : 'eval-cheap-source-map',
};
