/* @ngInject */
export default function runner($rootScope, $location, $log, AuthenticationService) {
  $rootScope.$on('$routeChangeStart', (event, next, current) => {// eslint-disable-line
    if (!AuthenticationService.getUser()) {
      $location.path('/login'); // event.preventDefault();
    }
  });
  $rootScope.$on('$routeChangeError', (event, current, previous, rejection) => {
    $log.warn('route change error...');
    switch (rejection) {
      default:
        $log.warn('$stateChangeError event catched');
        $location.path('/login');
        break;
    }
  });
}
