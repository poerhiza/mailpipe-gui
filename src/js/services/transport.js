/* @ngInject */
export default function TransportService(config, $http, $window) {
  this.$window = $window;

  this.get = (accounthash) => $http.get(
    `${config.api}account/transport/${accounthash}/`,
    { withCredentials: true },
  );

  this.add = (accounthash, transport) => $http.post(
    `${config.api}account/transport/${accounthash}/`,
    {
      data: transport,
      title: transport.title,
    },
    { withCredentials: true },
  );

  this.delete = (accounthash, transport) => $http.post(
    `${config.api}account/transport/${accounthash}/delete/`,
    {
      title: transport.title,
    },
    { withCredentials: true },
  );

  this.str2ab = (str) => {
    const array = new Uint8Array(str.length);

    for (let i = 0; i < str.length; i += 1) {
      array[i] = str.charCodeAt(i);
    }

    return array.buffer;
  };

  this.AESGCMEncrypt = (key, data, ad) => new Promise((resolve, reject) => {
    this.$window.crypto.subtle.importKey(
      'raw',
      this.str2ab(key),
      { name: 'AES-GCM' },
      false,
      ['encrypt'],
    ).then((cryptoKey) => {
      const initializationVector = this.$window.crypto.getRandomValues(new Uint8Array(12));

      this.$window.crypto.subtle.encrypt(
        { // https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/encrypt
          name: 'AES-GCM',
          iv: initializationVector,
          additionalData: this.str2ab(ad),
          tagLength: 128,
        },
        cryptoKey,
        this.str2ab(data),
      ).then((encrypted) => {
        const iv = Array.from(initializationVector).map((b) => b.toString(16).padStart(2, '0')).join('');
        const ct = Array.from(new Uint8Array(encrypted)).map((b) => b.toString(16).padStart(2, '0')).join('');

        resolve(`${iv}:${ct}`);
      })
        .catch((err) => {
          reject(err);
        });
    }).catch((err) => {
      reject(err);
    });
  });
}
