/* @ngInject */
export default function LinkService(config, $http) {
  this.get = (email) => $http.post(
    `${config.api}account/state/link/request`,
    { email },
    { withCredentials: true },
  );

  this.listLinkRequests = () => $http.get(
    `${config.api}account/state/link/request`,
    { withCredentials: true },
  );

  this.getLinkedAccounts = () => $http.get(
    `${config.api}account/state/link/`,
    { withCredentials: true },
  );

  this.replyRequestLink = (email, action) => $http.post(
    `${config.api}account/state/link/reply/${action}`,
    { email },
    { withCredentials: true },
  );

  this.deleteLink = (email, perspective) => $http.post(
    `${config.api}account/state/link/remove/${perspective}`,
    { email },
    { withCredentials: true },
  );
}
