/* @ngInject */
export default function ContactService(config, $http) {
  this.query = (emailhash, searchtext) => $http.post(
    `${config.api}account/contacts/${emailhash}/`,
    { query: searchtext },
    { withCredentials: true },
  );
}
