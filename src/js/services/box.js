/* @ngInject */
export default function InboxService(config, $http, $window) {
  this.get = (email, box) => $http.get(
    `${config.api}account/view/${email}/box/${box}/`,
    { withCredentials: true },
  );

  this.boxes = (emailhash) => $http.get(
    `${config.api}account/view/${emailhash}/boxes`,
    { withCredentials: true },
  );

  // TODO: refactor to use http and the method param so that the code is consice...

  this.getMessage = (email, box, id) => $http.get(
    `${config.api}account/view/${email}/box/${box}/message/${id}`,
    { withCredentials: true },
  );

  this.deleteMessages = (email, box, messages, purge) => $http.post(
    `${config.api}account/view/${email}/box/${box}/message/?purge=${purge}`,
    { messages },
    { withCredentials: true },
  );

  this.getAttachments = (email, box, id) => $http.get(
    `${config.api}account/view/${email}/box/${box}/message/${id}/download/attachments`,
    { withCredentials: true },
  );

  this.downloadAttachment = (email, box, emailId, attachmentId) => $window.open(`${config.api}account/view/${email}/box/${box}/message/${emailId}/download/attachment/${attachmentId}`, '_blank');
}
