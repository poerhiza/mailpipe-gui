/* @ngInject */
export default function EmailService(config, $http) {
  this.sendEmail = (emailhash, email, transport) => $http.post(
    `${config.api}account/send/${emailhash}/${transport}`,
    email,
    { withCredentials: true },
  );
}
