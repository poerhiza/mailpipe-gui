import { md5, sha3 } from 'hash-wasm';

/* @ngInject */
export default function AuthenticationService(config, $http, $window, $location) {
  this.$location = $location;

  try {
    this.user = JSON.parse($window.sessionStorage.getItem('mailpipe-user'));
  } catch (e) {
    this.user = null;
  }

  this.getUser = () => this.user;

  // eslint-disable-next-line max-len
  this.defaultPassword = async () => (this.user.password === await sha3(this.user.email, 512));

  this.change = async (current, latest) => $http.put(
    `${config.api}account/state/authentication/password`,
    {
      password: await sha3(current, 512),
      newpassword: await sha3(latest, 512),
    },
    { withCredentials: true },
  );

  this.authenticate = async (user, action) => {
    if (action === 'login') {
      this.user = JSON.parse(JSON.stringify(user));
      this.user.password = await sha3(this.user.password, 512);
      this.user.tk = await md5(user.password + this.user.password);
      this.user.hash = await sha3(this.user.email + this.user.password, 512);// eslint-disable-line
    }

    return $http.post(
      `${config.api}account/state/authentication/${action}`,
      {
        email: this.user.email,
        password: this.user.password,
        newpassword: '',
        tk: this.user.tk,
      },
      { withCredentials: true },
    ).then((response) => {
      if (response.data.success) {
        if (action === 'login') {
          this.user.unread = response.data.unread;

          $window.sessionStorage.setItem('mailpipe-user', JSON.stringify(this.user));

          return this.defaultPassword().then((usingDefault) => {
            if (usingDefault) {
              return 'password';
            }

            return true;
          });
        }

        if (action === 'logout') {
          delete this.user.authenticate;
          $window.sessionStorage.removeItem('mailpipe-user');
          // eslint-disable-next-line no-param-reassign
          $window.location.href = '/';
          return true;
        }
      }

      this.user = null;
      $window.sessionStorage.removeItem('mailpipe-user');
      // eslint-disable-next-line no-param-reassign
      $window.location.href = '/';

      return false;
    });
  };
}
