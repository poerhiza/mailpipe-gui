import 'angular-ui-grid/ui-grid.min.css';
import 'angular-material/angular-material.min.css';
import 'font-awesome/css/font-awesome.min.css';

import '../css/index.css';

import router from './route';
import runner from './runner';

import BoxCtrl from './controllers/box';
import LinkCtrl from './controllers/link';
import UserAuthCtrl from './controllers/userauth';
import ToolbarCtrl from './controllers/toolbar';
import PasswordCtrl from './controllers/password';
import EmailCtrl from './controllers/email';
import TransportCtrl from './controllers/transport';

import ToolbarDirective from './directives/toolbar';

import AuthenticationService from './services/authentication';
import BoxService from './services/box';
import LinkService from './services/link';
import EmailService from './services/email';
import ContactService from './services/contact';
import TransportService from './services/transport';

const angular = require('./angular-wrapper');//eslint-disable-line

const angularAria = require('angular-aria/angular-aria.min.js');//eslint-disable-line
const angularMaterial = require('angular-material/angular-material.min.js');//eslint-disable-line
const angularRoute = require('angular-route/angular-route.min.js');//eslint-disable-line
const angularAnimate = require('angular-animate/angular-animate.min.js');//eslint-disable-line

const uiGridCore = require('angular-ui-grid/ui-grid.core.min.js');//eslint-disable-line
const uiGridAutoResize = require('angular-ui-grid/ui-grid.auto-resize.min.js');//eslint-disable-line
const uiGridSelection = require('angular-ui-grid/ui-grid.selection.min.js');//eslint-disable-line
const uiGridCellNav = require('angular-ui-grid/ui-grid.cellnav.min.js');//eslint-disable-line

export default angular.module(
  'mailpipe',
  [
    'ngAnimate',
    'ngMaterial',
    'ngAria',
    'ngRoute',
    'ui.grid',
    'ui.grid.autoResize',
    'ui.grid.selection',
    'ui.grid.cellNav',
  ],
)
  .config(router)
  .run(runner)
  .controller('BoxCtrl', BoxCtrl)
  .controller('LinkCtrl', LinkCtrl)
  .controller('UserAuthCtrl', UserAuthCtrl)
  .controller('ToolbarCtrl', ToolbarCtrl)
  .controller('PasswordCtrl', PasswordCtrl)
  .controller('EmailCtrl', EmailCtrl)
  .controller('TransportCtrl', TransportCtrl)
  .directive('toolbarDirective', ToolbarDirective)
  .service('AuthenticationService', AuthenticationService)
  .service('BoxService', BoxService)
  .service('LinkService', LinkService)
  .service('EmailService', EmailService)
  .service('ContactService', ContactService)
  .service('TransportService', TransportService)
  .constant('config', { /* global SERVICE_URL */
    api: `${SERVICE_URL}/api/v1/`,
  })
  .name;
