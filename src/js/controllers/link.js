/* @ngInject */
class LinkCtrl {
  /**
     * Constructor class HomeController
     *
     * @param {object} $scope
     */
  constructor($scope, $log, $mdToast, LinkService, AuthenticationService) {
    'ngInject';

    this.$mdToast = $mdToast;
    this.$scope = $scope;
    this.$log = $log;
    this.AuthenticationService = AuthenticationService;
    this.LinkService = LinkService;

    this.user = this.AuthenticationService.getUser();
    this.requestedAccountLinks = [];
    this.linkedAccounts = [];

    this.classUp = {
      pending: 'fa-thumbs-o-up',
      accepted: 'fa-thumbs-up',
      denied: 'fa-thumbs-o-up',
    };
    this.classDown = {
      pending: 'fa-thumbs-o-down',
      accepted: 'fa-thumbs-o-down',
      denied: 'fa-thumbs-down',
    };

    this.LinkService.getLinkedAccounts().then((response) => {
      Object.keys(response.data.accounts).forEach((email) => {
        this.linkedAccounts.push({ email });
      });
    });

    this.LinkService.listLinkRequests().then((response) => {
      Object.keys(response.data.requests).forEach((k) => {
        this.requestedAccountLinks.push({
          email: k,
          status: response.data.requests[k],
          classUp: this.classUp[response.data.requests[k]],
          classDown: this.classDown[response.data.requests[k]],
        });
      });
    });

    this.request = {
      email: '',
    };
    this.errorMessage = false;
  }

  deleteLink(link, perspective) {
    this.LinkService.deleteLink(link.email, perspective);

    if (perspective === 'link') {
      this.linkedAccounts = this.linkedAccounts.filter((item) => item.email !== link.email);
    }

    if (perspective === 'access') {
      this.requestedAccountLinks = this.requestedAccountLinks.filter((item) => item.email !== link.email);//eslint-disable-line
    }
  }

  setLinkStatus(link, status) {
    this.LinkService.replyRequestLink(link.email, status);
    this.requestedAccountLinks.forEach((obj) => { // TODO: better way of doing this?
      if (obj.email === link.email) {
        obj.status = status;//eslint-disable-line
        obj.classUp = this.classUp[status];//eslint-disable-line
        obj.classDown = this.classDown[status];//eslint-disable-line
      }
    });
  }

  requestAccountLink() {
    this.$log.log(this.request, this.user);
    this.LinkService.get(this.request.email).then((response) => {
      this.$log.log(response);
      if (response.data.success) {
        this.errorMessage = '';
        this.request.email = '';
        this.$scope.emailRequestLink.$setPristine();
        this.$scope.emailRequestLink.$setUntouched();
        const notification = this.$mdToast.simple();
        notification.textContent('Link request sent!');
        notification.position('top left');
        notification.action('Okay');
        notification.capsule(true);
        this.$mdToast.show(notification);
      } else {
        this.errorMessage = 'Something went wrong - check the address and try again...';
      }
    }).catch((response) => {
      this.errorMessage = response.data.message;
    });
  }
}

export default LinkCtrl;
