import * as formatterHtml from 'html-formatter';
import wrapAnsi from 'wrap-ansi';

import { md5 } from 'hash-wasm';

/* @ngInject */
class BoxCtrl {
  /**
     * Constructor class HomeController
     *
     * @param {object} $scope
     */
  constructor(
    AuthenticationService,
    BoxService,
    LinkService,
    uiGridConstants,
    $scope,
    $log,
    $mdSidenav,
    $window,
    $location,
  ) {
    'ngInject';

    this.uiGridConstants = uiGridConstants;
    this.AuthenticationService = AuthenticationService;
    this.LinkService = LinkService;
    this.BoxService = BoxService;
    this.$log = $log;
    this.$mdSidenav = $mdSidenav;
    this.$window = $window;
    this.$location = $location;
    this.attachments = [];
    this.keyFilter = {
      text: 'Text',
      html: 'HTML',
      errors: 'Errors',
    };

    this.user = AuthenticationService.getUser();
    this.searchText = this.user.email;

    this.accountView = this.user;
    this.boxListing = {};

    this.linkedAccounts = [
      {
        email: this.user.email,
        hash: this.user.hash,
        unread: this.user.unread,
      },
    ];

    this.LinkService.getLinkedAccounts().then((response) => {
      Object.keys(response.data.accounts).forEach((email) => {
        this.linkedAccounts.push({
          email,
          hash: response.data.accounts[email].hash,
          unread: response.data.accounts[email].unread,
        });
      });

      this.boxListing = {};

      const boxlisting = {};

      this.linkedAccounts.forEach((entry) => {
        const emailsplit = entry.email.split('@');
        const handle = emailsplit[0];
        const domain = emailsplit[1];

        if (this.boxListing[domain] === undefined) {
          this.boxListing[domain] = [];
        }
        if (boxlisting[domain] === undefined) {
          boxlisting[domain] = [];
        }
        // eslint-disable-next-line no-param-reassign
        entry.handle = handle;
        boxlisting[domain].push(entry);
      });

      Object.keys(boxlisting).forEach((domain) => {
        const allread = [];

        boxlisting[domain].forEach((account) => {
          if (account.unread > 0) {
            this.boxListing[domain].push(account);
          } else {
            allread.push(account);
          }
        });

        this.boxListing[domain] = this.boxListing[domain].concat(allread);
      });
    });

    this.purgeAllBoxes = true;
    this.inbox = [];
    this.boxes = ['inbox'];
    this.selectedBox = 'inbox';
    this.gridOptions = {
      exporterMenuCsv: true,
      enableGridMenu: true,
      enableRowSelection: true,
      enableSelectAll: true,
      selectionRowHeaderWidth: 35,
      rowHeight: 35,
      showGridFooter: true,
      multiSelect: true,
      enableFiltering: true,
      enableHiding: false,
      enableSorting: true,
      appScopeProvider: this,
      rowTemplate: '<div ng-click="grid.appScope.viewInboxMessage(row)" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.uid" class="ui-grid-cell" ng-class="col.colIndex()" ui-grid-cell></div>',
      columnDefs: [
        {
          name: 'subject',
          cellClass: (grid, row, col) => {// eslint-disable-line
            let ret = 'email-unread';

            if (row.entity.read) {
              ret = 'email-read';
            }

            return ret;
          },
        },
        {
          name: 'datetime',
          type: 'date',
          cellFilter: 'date: \'yyyy-MM-ddTHH:mm:ssZ\'',
          sort: {
            direction: 'desc',
            priority: 0,
          },
        },
      ],
      data: this.inbox,
      onRegisterApi: (gridApi) => {
        this.gridApi = gridApi;

        gridApi.selection.on.rowSelectionChanged($scope, (row) => {//eslint-disable-line
          this.hasRowsSelected = gridApi.selection.getSelectedRows().length;
        });
        gridApi.selection.on.rowSelectionChangedBatch($scope, (rows) => {//eslint-disable-line
          this.hasRowsSelected = gridApi.selection.getSelectedRows().length;
        });
      },
    };

    this.loading = true;

    if (this.user.email.indexOf('*@') === 0) {
      this.gridOptions.columnDefs.unshift({ name: 'email' });
    } // A domain wide account should see the email address the email was sent to

    BoxService.boxes(this.user.hash).then((response) => {
      if (response.data.boxes) {
        this.boxes = response.data.boxes;
        this.selectedBox = this.boxes[0];// eslint-disable-line
      }
    }).finally(() => {
      this.loading = false;

      this.loadBox(this.selectedBox);
    });
  }

  loadBox(box) {
    this.loading = true;
    this.BoxService.get(this.user.hash, box).then((response) => {
      this.inbox = response.data.items;
      this.gridOptions.data = this.inbox;
      this.selectedBox = box;
    }).catch(() => {
      // TODO: logging?
    }).finally(() => {
      this.loading = false;
    });
  }

  toggleRight() {
    this.$mdSidenav('right')
      .toggle();
  }

  isOpenRight() {
    return this.$mdSidenav('right').isOpen();
  }

  close() {
    this.$mdSidenav('right').close();
  }

  renderHTML(data) {
    const emailRenderWindow = this.$window.open('127.0.0.1', '_blank');

    emailRenderWindow.document.write(data);
    emailRenderWindow.opener = null;
  }

  reply(action) {
    this.gridOptions.appScopeProvider.gridApi.grid.rows.forEach((r) => {
      if (r.isSelected && r.entity.id) {
        this.$location.path(`send/${action}/${this.user.hash}/${this.selectedBox}/${r.entity.id}`);
      }
    });
  }

  delete() {
    const messageIds = [];
    const messages = [];

    this.gridOptions.appScopeProvider.gridApi.grid.rows.forEach((r) => {
      if (r.isSelected) {
        messageIds.push(r.entity.id);
        messages.push({
          id: r.entity.id,
          subject: r.entity.subject,
          datetime: r.entity.datetime,
          email: r.entity.email,
        });
      }
    });

    this.loading = true;

    this.BoxService.deleteMessages(
      this.user.hash,
      this.selectedBox,
      messages,
      this.purgeAllBoxes,
    ).then(() => { // TODO: add UI element for purge setting (true atm...)
      this.linkedAccounts.forEach((entry) => {
        if (entry.hash === this.accountView.hash) {
          if (entry.unread > 0) {
            // eslint-disable-next-line no-param-reassign
            entry.unread -= messages.length;
          }
        }
      });

      this.gridOptions.data = this.gridOptions.data.filter((r) => messageIds.indexOf(r.id) === -1);
    }).finally(() => {
      this.loading = false;
    });
  }

  viewInboxMessage(row) {//eslint-disable-line

    row.grid.rows.forEach((r) => {
      r.isSelected = false;//eslint-disable-line
    });

    row.isSelected = !row.isSelected;//eslint-disable-line

    if (this.isOpenRight()) {
      this.toggleRight();
    }

    if (row.isSelected) {
      this.gridApi.selection.clearSelectedRows();
      this.selectedRow = row;
      this.gridApi.selection.selectRow(this.selectedRow.entity, null);

      this.BoxService.getMessage(
        this.user.hash,
        this.selectedBox,
        row.entity.id,
      ).then((response) => {
        // assume success?

        if (!this.selectedRow.entity.read) {
          this.linkedAccounts.forEach((entry) => {
            if (entry.hash === this.accountView.hash) {
              if (entry.unread > 0) {
                // eslint-disable-next-line no-param-reassign
                entry.unread -= 1;
              }
            }
          });

          // eslint-disable-next-line no-param-reassign
          row.entity.read = true;
          this.gridApi.core.notifyDataChange(this.uiGridConstants.dataChange.EDIT);
        }

        // this.$log.log(response);
        this.selectedIndex = 0;
        this.tabs = [];

        Object.keys(this.keyFilter).forEach((k) => {
          let data = response.data[k];

          if (data) {
            if (k === 'html') {
              data = formatterHtml.render(data).replace(/\t/g, '  ');
            }

            if (k === 'text') {
              data = wrapAnsi(data, (this.$window.innerWidth) / 'A'.width());
            }

            this.tabs.push({
              title: this.keyFilter[k],
              id: k,
              content: data,
            });
          }
        });

        const headers = [];
        let attachmentCount = 0;

        Object.keys(response.data.headers).forEach((key) => {
          if (key === 'Attachments' && response.data.headers[key] > 0) {
            attachmentCount = response.data.headers[key];
          } else {
            headers.push({
              key,
              value: response.data.headers[key],
            });
          }
        });

        this.tabs.push({
          title: 'Headers',
          id: 'headers',
          content: {
            rowHeight: 35,
            showGridFooter: true,
            enableFiltering: true,
            enableHiding: false,
            enableSorting: true,
            columnDefs: [
              { name: 'key', width: '25%' },
              { name: 'value' },
            ],
            data: headers,
          },
        });

        if (attachmentCount > 0) {
          this.BoxService.getAttachments(
            this.user.hash,
            this.selectedBox,
            row.entity.id,
          ).then((attachmentsResponse) => {
            const files = [];

            attachmentsResponse.data.forEach((item) => {
              files.push({ filename: item, emailId: row.entity.id });
            });

            this.tabs.push({
              title: 'Attachments',
              id: 'attachments',
              attachments: files,
            });
          }).finally(() => {
            this.$log.log('finished getting attachments ; )');
          });
        }

        this.toggleRight();
      }).catch((err) => {
        this.$log.log(err);
        // TODO: show toast 'failed to load message?'
      });
    }
  }

  async downloadAttachment(attachment) {
    const attachmentKey = await md5(attachment.filename);
    this.BoxService.downloadAttachment(
      this.user.hash,
      this.selectedBox,
      attachment.emailId,
      attachmentKey,
    );
  }

  createFilterFor(query) {// eslint-disable-line
    const lowercaseQuery = query.toLowerCase();

    return function filterFn(account) {
      return (account.email.indexOf(lowercaseQuery) === 0);
    };
  }

  querySearch(query) {
    return query ? this.linkedAccounts.filter(this.createFilterFor(query)) : this.linkedAccounts;
  }

  updateView(account) {
    if (account !== undefined && !this.loading) {
      this.loading = true;

      this.linkedAccounts.forEach((item) => {
        if (item.email === account.email) {
          this.user = item;
          this.accountView = account;
          this.searchText = account.email;
        }
      });

      this.BoxService.boxes(this.user.hash).then((response) => {
        if (response.data.boxes) {
          this.boxes = response.data.boxes;
          this.selectedBox = this.boxes[0];//eslint-disable-line
        }
      }).finally(() => {
        this.loading = false;

        this.loadBox(this.selectedBox);
      });
    }
  }
}

export default BoxCtrl;
