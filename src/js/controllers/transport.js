/* @ngInject */
class TransportCtrl {
  /**
     * Constructor class TransportCtrl
     *
     * @param {object} $scope
     */
  constructor($scope, $log, $mdToast, TransportService, AuthenticationService) {
    'ngInject';

    this.$mdToast = $mdToast;
    this.$scope = $scope;
    this.$log = $log;
    this.TransportService = TransportService;
    this.AuthenticationService = AuthenticationService;

    this.user = this.AuthenticationService.getUser();
    this.fetchTransports();

    this.transportTypes = {
      simple: {
        title: '',
        host: '',
        username: '',
        password: '',
        port: 25,
        footer: '',
      },
      sendgrid: {
        title: '',
        url: 'https://api.sendgrid.com:443/v3/mail/send',
        apikey: '',
        footer: 'This e-mail and any attachments it may contain are confidential and privileged information. If you are not the intended recipient, please notify the sender immediately by return e-mail, delete this e-mail and destroy any copies. Any dissemination or use of this information by any other than the intended recipient is unauthorized and may be illegal.',
      },
    };

    this.types = Object.keys(this.transportTypes);

    this.emptyTransport = this.transportTypes.simple;

    this.transport = this.newTransport();

    this.errorMessage = false;
  }

  fetchTransports() {
    return this.TransportService.get(this.user.hash).then((response) => {
      this.transports = response.data.transports;
      return this.transports;
    }).catch((response) => {
      const notification = this.$mdToast.simple();
      notification.textContent('Failed to fetch site-wide transports!');
      notification.position('top left');
      notification.action('Sad day');
      notification.capsule(true);
      this.$mdToast.show(notification);

      console.error(response); // eslint-disable-line

      return [];
    });
  }

  newTransport() {
    return JSON.parse(JSON.stringify(this.emptyTransport));
  }

  encrypt(t) {
    const transport = JSON.parse(JSON.stringify(t));

    return new Promise((resolve, reject) => {
      if (transport.type === 'simple') {
        if (transport.username !== '') {
          this.TransportService.AESGCMEncrypt(
            this.user.tk,
            transport.username,
            this.user.email,
          ).then((usernameCiphertext) => {
            transport.username = usernameCiphertext;

            if (transport.password !== '') {
              this.TransportService.AESGCMEncrypt(
                this.user.tk,
                transport.password,
                this.user.email,
              ).then((passwordCiphertext) => {
                transport.password = passwordCiphertext;
                resolve(transport);
              }).catch((failure) => {
                reject(failure);
              });
            } else {
              resolve(transport);
            }
          }).catch((failure) => {
            reject(failure);
          });
        } else {
          resolve(transport);
        }
      }

      if (transport.type === 'sendgrid') {
        this.TransportService.AESGCMEncrypt(
          this.user.tk,
          transport.apikey,
          this.user.email,
        ).then((keyCiphertext) => {
          transport.apikey = keyCiphertext;
          resolve(transport);
        }).catch((failure) => {
          reject(failure);
        });
      }
    });
  }

  add(transport) {
    this.encrypt(transport).then((encryptedTransport) => {
      this.TransportService.add(this.user.hash, encryptedTransport).then((response) => {
        if (response.status === 200) {
          this.transport = this.newTransport();
          this.errorMessage = false;
          this.fetchTransports();
          this.$scope.addTransportForm.$setPristine();
          this.$scope.addTransportForm.$setUntouched();
        }
      }).catch(() => {
        this.errorMessage = 'Failed to add transport!';
        const notification = this.$mdToast.simple();
        notification.textContent(this.errorMessage);
        notification.position('top left');
        notification.action('Sad day');
        notification.capsule(true);
        this.$mdToast.show(notification);

        return [];
      });
    }).catch(() => {
      this.errorMessage = 'Failed to add transport!';
      const notification = this.$mdToast.simple();
      notification.textContent(this.errorMessage);
      notification.position('top left');
      notification.action('Sad day');
      notification.capsule(true);
      this.$mdToast.show(notification);
    });
  }

  delete(transport) {
    this.TransportService.delete(this.user.hash, transport).then((response) => {
      if (response.status === 200) {
        this.fetchTransports();
        this.errorMessage = false;
      }
    }).catch(() => {
      this.errorMessage = 'Failed to add transport!';
      const notification = this.$mdToast.simple();
      notification.textContent(this.errorMessage);
      notification.position('top left');
      notification.action('Sad day');
      notification.capsule(true);
      this.$mdToast.show(notification);
      return [];
    });
  }
}

export default TransportCtrl;
