/* @ngInject */
class PasswordCtrl {
  /**
     * Constructor class PasswordController
     *
     * @param {object} $scope
     */
  constructor($scope, $log, $mdToast, AuthenticationService, $location) {
    'ngInject';

    this.$mdToast = $mdToast;
    this.$scope = $scope;
    this.$log = $log;

    this.AuthenticationService = AuthenticationService;
    this.$location = $location;

    this.user = this.AuthenticationService.getUser();

    this.usingDefault = AuthenticationService.defaultPassword();

    this.request = {
      currentPassword: '',
      newPassword: '',
      newPasswordConfirm: '',
    };
    this.newRequest = {
      currentPassword: '',
      newPassword: '',
      newPasswordConfirm: '',
    };
    this.errorMessage = false;

    if (this.usingDefault) {
      this.request.currentPassword = this.user.email;
      this.errorMessage = 'Please change the default password...';
    }
  }

  changePassword() {
    this.$log.log(this.request);
    this.AuthenticationService.change(
      this.request.currentPassword,
      this.request.newPassword,
    ).catch((response) => {
      this.errorMessage = response.data.message;
    }).then((response) => {
      if (response.data.success) {
        this.request = this.newRequest;
        this.errorMessage = '';
        this.$scope.passwordChangeForm.$setPristine();
        this.$scope.passwordChangeForm.$setUntouched();
        const notification = this.$mdToast.simple();
        notification.textContent('Password changed!');
        notification.position('top left');
        notification.action('Okay');
        notification.capsule(true);
        this.$mdToast.show(notification);

        if (this.usingDefault) {
          this.errorMessage = '';
        }

        setTimeout(() => {
          this.$location.path('');
        }, 2000);
      } else {
        this.errorMessage = 'Something went wrong - check the address and try again...';
      }
    });
  }
}

export default PasswordCtrl;
