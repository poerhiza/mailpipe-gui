import * as formatterHtml from 'html-formatter';
import * as wrapAnsi from 'wrap-ansi';

import { md5 } from 'hash-wasm';

/* @ngInject */
class MailpipeCtrl {
  /**
     * Constructor class HomeController
     *
     * @param {object} $scope
     */
  constructor(AuthenticationService, BoxService, $scope, $log, $mdSidenav, $window, $location) {
    'ngInject';

    this.AuthenticationService = AuthenticationService;
    this.BoxService = BoxService;
    this.$log = $log;
    this.$mdSidenav = $mdSidenav;
    this.$window = $window;
    this.$location = $location;
    this.attachments = [];
    this.keyFilter = {
      text: 'Text',
      html: 'HTML',
      errors: 'Errors',
    };

    this.purgeAllBoxes = true;
    this.user = AuthenticationService.getUser();
    this.inbox = [];
    this.boxes = ['inbox'];
    this.selectedBox = 'inbox';
    this.gridOptions = {
      exporterMenuCsv: true,
      enableGridMenu: true,
      enableRowSelection: true,
      enableSelectAll: true,
      selectionRowHeaderWidth: 35,
      rowHeight: 35,
      showGridFooter: true,
      multiSelect: true,
      enableFiltering: true,
      enableHiding: false,
      enableSorting: true,
      appScopeProvider: this,
      rowTemplate: '<div ng-click="grid.appScope.viewInboxMessage(row)" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.uid" class="ui-grid-cell" ng-class="col.colIndex()" ui-grid-cell></div>',
      columnDefs: [
        { name: 'subject' },
        { name: 'datetime', type: 'date', cellFilter: 'date: \'yyyy-MM-ddTHH:mm:ssZ\'' },
      ],
      data: this.inbox,
      onRegisterApi: (gridApi) => {
        this.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, (row) => {
          this.hasRowsSelected = row.isSelected;
        });
      },
    };

    this.loading = true;

    if (this.user.email.indexOf('*@') === 0) {
      this.gridOptions.columnDefs.unshift({ name: 'email' });
    } // A domain wide account should see the email address the email was sent to

    BoxService.boxes(this.user.hash).then((response) => {
      if (response.data.boxes) {
        this.boxes = response.data.boxes;
      }
    }).finally(() => {
      this.loading = false;
    });

    this.loadBox(this.selectedBox);
  }

  loadBox(box) {
    this.loading = true;
    this.BoxService.get(this.user.hash, box).then((response) => {
      this.inbox = response.data.items;
      this.gridOptions.data = this.inbox;
      this.selectedBox = box;
    }).catch(() => {
      // TODO: logging?
    }).finally(() => {
      this.loading = false;
    });
  }

  toggleRight() {
    this.$mdSidenav('right')
      .toggle();
  }

  isOpenRight() {
    return this.$mdSidenav('right').isOpen();
  }

  close() {
    this.$mdSidenav('right').close();
  }

  delete() {
    const messageIds = [];
    const messages = [];

    this.gridOptions.appScopeProvider.gridApi.grid.rows.forEach((r) => {
      if (r.isSelected) {
        messageIds.push(r.entity.id);
        messages.push({
          id: r.entity.id,
          subject: r.entity.subject,
          datetime: r.entity.datetime,
          email: r.entity.email,
        });
      }
    });

    this.loading = true;

    this.BoxService.deleteMessages(
      this.user.hash,
      this.selectedBox,
      messages,
      this.purgeAllBoxes,
    ).then(() => { // TODO: add UI element for purge setting (true atm...)
      this.gridOptions.data = this.gridOptions.data.filter((r) => messageIds.indexOf(r.id) === -1);
    }).finally(() => {
      this.loading = false;
    });
  }

  viewInboxMessage(row) {//eslint-disable-line

    row.grid.rows.forEach((r) => {
      r.isSelected = false;//eslint-disable-line
    });

    row.isSelected = !row.isSelected;//eslint-disable-line

    if (this.isOpenRight()) {
      this.toggleRight();
    }

    if (row.isSelected) {
      this.selectedRow = row;

      this.BoxService.getMessage(
        this.user.hash,
        this.selectedBox,
        row.entity.id,
      ).then((response) => {
        this.$log.log(response);
        this.selectedIndex = 0;
        this.tabs = [];

        Object.keys(this.keyFilter).forEach((k) => {
          let data = response.data[k];

          if (data) {
            if (k === 'html') {
              data = formatterHtml.render(data).replace(/\t/g, '  ');
            }

            if (k === 'text') {
              data = wrapAnsi(data, (this.$window.innerWidth) / 'A'.width());
            }

            this.tabs.push({
              title: this.keyFilter[k],
              id: k,
              content: data,
            });
          }
        });

        const headers = [];
        let attachmentCount = 0;

        Object.keys(response.data.headers).forEach((key) => {
          if (key === 'Attachments' && response.data.headers[key] > 0) {
            attachmentCount = response.data.headers[key];
          } else {
            headers.push({
              key,
              value: response.data.headers[key],
            });
          }
        });

        this.tabs.push({
          title: 'Headers',
          id: 'headers',
          content: {
            rowHeight: 35,
            showGridFooter: true,
            enableFiltering: true,
            enableHiding: false,
            enableSorting: true,
            columnDefs: [
              { name: 'key', width: '25%' },
              { name: 'value' },
            ],
            data: headers,
          },
        });

        if (attachmentCount > 0) {
          this.BoxService.getAttachments(
            this.user.hash,
            this.selectedBox,
            row.entity.id,
          ).then((attachmentsResponse) => {
            const files = [];

            attachmentsResponse.data.forEach((item) => {
              files.push({ filename: item, emailId: row.entity.id });
            });

            this.tabs.push({
              title: 'Attachments',
              id: 'attachments',
              attachments: files,
            });
          }).finally(() => {
            this.$log.log('finished getting attachments ; )');
          });
        }

        this.toggleRight();
      }).catch((err) => {
        this.$log.log(err);
        // TODO: show toast 'failed to load message?'
      });
    }
  }

  async downloadAttachment(attachment) {
    const attachmentKey = await md5(attachment.filename);
    this.BoxService.downloadAttachment(
      this.user.hash,
      this.selectedBox,
      attachment.emailId,
      attachmentKey,
    );
  }

  logout() {
    this.AuthenticationService.authenticate(this.user, 'logout');
  }
}

export default MailpipeCtrl;
