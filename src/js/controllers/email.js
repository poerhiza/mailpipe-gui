const jdenticon = require('jdenticon/standalone');
const svgToDataurl = require('svg-to-dataurl');

/* @ngInject */
class EmailCtrl {
  /**
     * Constructor class EmailCtrl
     *
     * @param {object} $scope
     */
  constructor(
    $q,
    $scope,
    $log,
    $mdToast,
    $timeout,
    $routeParams,
    AuthenticationService,
    ContactService,
    EmailService,
    LinkService,
    BoxService,
    TransportService,
  ) {
    'ngInject';

    this.$mdToast = $mdToast;
    this.$timeout = $timeout;
    this.$routeParams = $routeParams;
    this.$scope = $scope;
    this.$log = $log;
    this.$q = $q;
    this.AuthenticationService = AuthenticationService;
    this.ContactService = ContactService;
    this.EmailService = EmailService;
    this.LinkService = LinkService;
    this.BoxService = BoxService;
    this.TransportService = TransportService;

    this.user = this.AuthenticationService.getUser();
    this.accountView = this.user.email;
    this.transports = [];
    this.selectedTransport = false;

    this.errorMessage = false;
    this.emptyEmail = {
      to: [],
      cc: [],
      bcc: [],
      subject: '',
      content: '',
    };
    this.linkedAccounts = [
      { email: this.user.email, hash: this.user.hash },
    ];

    this.LinkService.getLinkedAccounts().then((response) => {
      Object.keys(response.data.accounts).forEach((email) => {
        this.linkedAccounts.push({ email, hash: response.data.accounts[email] });
      });
    });

    this.email = this.newEmail();
    this.bccenabled = false;

    this.emailLineToFakeEntry = (splitEmailLine) => {
      const email = splitEmailLine.pop().split('>').shift();

      let name = '';

      if (splitEmailLine !== undefined) {
        name = splitEmailLine.shift();
      }

      if (name === undefined) {
        name = email;
      }

      return {
        image: svgToDataurl(jdenticon.toSvg(email, 50)),
        email,
        name,
      };
    };

    this.TransportService.get(this.user.hash).then((response) => {
      this.transports = response.data.transports;
      this.selectedTransport = this.transports[0].id;
    }).catch((response) => {
      const notification = this.$mdToast.simple();
      notification.textContent('Failed to fetch site-wide transports!');
      notification.position('top left');
      notification.action('Sad day');
      notification.capsule(true);
      this.$mdToast.show(notification);
      // TODO: add error message in response and pass to notification
      console.error(response); // eslint-disable-line
    });
  }

  updateView() {
    this.loading = true;

    this.linkedAccounts.forEach((item) => {
      if (item.email === this.accountView) {
        this.user = item;
        this.loading = false;
      }
    });
  }

  toggleBCC() {
    this.bccenabled = !this.bccenabled;
  }

  newEmail() {
    const message = JSON.parse(JSON.stringify(this.emptyEmail));

    if (
      this.$routeParams.action.indexOf('reply') >= 0
      && this.$routeParams.email
      && this.$routeParams.box
      && this.$routeParams.messageId
    ) {
      this.BoxService.getMessage(this.$routeParams.email, this.$routeParams.box, this.$routeParams.messageId).then((response) => { // eslint-disable-line
        if (response.status === 200) {
          const from = response.data.headers.From.split('<');
          const subject = response.data.headers.Subject;
          const toline = response.data.headers.To;
          const content = response.data.text;

          if (from !== undefined) {
            message.to.push(this.emailLineToFakeEntry(from));
          }

          if (subject !== undefined) {
            message.subject = `RE: ${subject}`;
          }

          if (content !== undefined) {
            message.content = `

============================================
Date: ${response.data.headers.Date}
Author: ${response.data.headers.From}
Contents: ${content}
`;
          }

          if (this.$routeParams.action === 'replyall') {
            toline.split(',').forEach((entry) => {
              message.cc.push(this.emailLineToFakeEntry(entry.trim().split('<')));
            });
          }
        }
      }).catch((err) => {
        console.error(err); // eslint-disable-line
        const notification = this.$mdToast.simple();
        notification.textContent('Failed to pre-populate email');
        notification.position('top left');
        notification.action('Sad day');
        notification.capsule(true);
        this.$mdToast.show(notification);
      });
    }

    return message;
  }

  refreshDebounce() {
    this.lastSearch = 0;
    this.pendingSearch = null;
    this.cancelSearch = null;
  }

  debounceSearch() {
    const now = new Date().getMilliseconds();
    this.lastSearch = this.lastSearch || now;

    return ((now - this.lastSearch) < 500);
  }

  queryContacts(searchtext) {
    const deferred = this.$q.defer();

    this.ContactService.query(this.user.hash, searchtext).then((response) => {
      if (response.status === 200) {
        if (response.data.matches) {
          deferred.resolve(response.data.matches.map((item) => {
            item.image = svgToDataurl(jdenticon.toSvg(item.email, 50));//eslint-disable-line
            return item;
          }));
        } else {
          deferred.reject('no matches found...');
        }
      } else {
        this.errorMessage = response.data;
        deferred.reject(response);
      }
    });

    return deferred.promise;
  }

  reset() {
    this.email = JSON.parse(JSON.stringify(this.emptyEmail));
  }

  send() {
    const email = JSON.parse(JSON.stringify(this.emptyEmail));

    this.email.to.forEach((item) => {
      const to = item;
      delete to.image;
      email.to.push(to);
    });

    this.email.cc.forEach((item) => {
      const cc = item;
      delete cc.image;
      email.cc.push(cc);
    });

    this.email.bcc.forEach((item) => {
      const bcc = item;
      delete bcc.image;
      email.bcc.push(bcc);
    });

    email.subject = this.email.subject;
    email.text = this.email.content;

    const parseFiles = new Promise((resolve, reject) => {
      try {
        const fileItem = document.getElementById('emailAttachments');//eslint-disable-line
        this.attachmentFromSize = {};

        if (fileItem.files && fileItem.files.length) {
          let fileCount = fileItem.files.length;
          let c = fileItem.files.length;

          const attachments = [];

          for (; c > 0;) {//eslint-disable-line
            c -= 1;
            const file = fileItem.files[c];

            this.attachmentFromSize[file.size] = {
              type: file.type,
              name: file.name,
            };
            var reader = new FileReader();//eslint-disable-line

            reader.onload = (e) => {//eslint-disable-line
              fileCount -= 1;

              const item = {
                content: btoa(e.target.result),//eslint-disable-line
                size: e.target.result.length,
              };
              const entry = this.attachmentFromSize[item.size];

              if (entry) {
                item.type = entry.type;
                item.filename = entry.name;
              }
              attachments.push(item);

              if (fileCount <= 0) {
                resolve(attachments);
              }
            };
            reader.readAsBinaryString(file);
          }
        } else {
          resolve(false);
        }
      } catch (e) {
        reject(e);
      }
    });

    parseFiles.then((attachments) => {
      if (attachments) {
        email.attachments = attachments;
      }
      console.log(email);//eslint-disable-line

      this.EmailService.sendEmail(this.user.hash, email, this.selectedTransport)
        .catch((response) => {
          this.errorMessage = response.data;
        })
        .then((response) => {
          if (response) {
            this.$timeout(() => {
              const notification = this.$mdToast.simple();
              notification.textContent('E-Mail sent!');
              notification.position('top left');
              notification.action('Okay');
              notification.capsule(true);
              this.$mdToast.show(notification);
              this.email = JSON.parse(JSON.stringify(this.emptyEmail));
            }, 0);
          }
        });
    }).catch((err) => {
      this.$log.log('catch: ', err);
    });
  }
}

export default EmailCtrl;
