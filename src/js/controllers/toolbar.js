/* @ngInject */
class ToolbarCtrl {
  /**
     * Constructor class HomeController
     *
     * @param {object} $scope
     */
  constructor(AuthenticationService, $scope, $log, $location) {
    'ngInject';

    this.$log = $log;
    this.$scope = $scope;
    this.$location = $location;
    this.AuthenticationService = AuthenticationService;

    this.user = this.AuthenticationService.getUser();
  }

  logout() {
    this.AuthenticationService.authenticate(this.user, 'logout');
  }
}

export default ToolbarCtrl;
