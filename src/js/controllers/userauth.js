/* @ngInject */
export default function UserAuthCtrl(AuthenticationService, $location) {//eslint-disable-line

  if (AuthenticationService.getUser()) {
    $location.path('/');
  }

  const vm = this;

  vm.error = '';

  vm.user = {
    email: '',
    password: '',
  };

  vm.authenticate = () => {
    if (vm.user.email.indexOf('@') === -1) {
      vm.user.email = `${vm.user.email}@${$location.$$host}`;
    }

    if (vm.user.email) {
      if (vm.user.password === undefined) {
        vm.user.password = '';
      }

      AuthenticationService.authenticate({
        email: vm.user.email,
        password: vm.user.password,
      }, 'login').then((status) => {
        if (status === true) {
          $location.path('/');
        } else if (status === 'password') {
          $location.path('/password');
        } else {
          vm.error = 'Failed to authenticate with service - is it down?';
        }
      }).catch(() => {
        vm.error = 'Failed to authenticate';
      });
    }
  };
}
