/* @ngInject */
export default function router(
  $mdThemingProvider,
  $routeProvider,
  $locationProvider,
) {
  $mdThemingProvider.theme('default')
    .primaryPalette('green')
    .accentPalette('light-green');

  const boxes = {
    template: require('../views/boxes.html'),// eslint-disable-line
    controller: 'BoxCtrl as ctrl',
  };

  $routeProvider
    .when('/', boxes)
    .when('/boxes', boxes)
    .when('/links', {
      template: require('../views/links.html'),// eslint-disable-line
      controller: 'LinkCtrl as ctrl',
    })
    .when('/password', {
      template: require('../views/password.html'),// eslint-disable-line
      controller: 'PasswordCtrl as ctrl',
    })
    .when('/send/:action/:email/:box/:messageId', {
      template: require('../views/email.html'),// eslint-disable-line
      controller: 'EmailCtrl as ctrl',
    })
    .when('/send/', {
      template: require('../views/email.html'),// eslint-disable-line
      controller: 'EmailCtrl as ctrl',
    })
    .when('/transports', {
      template: require('../views/transports.html'),// eslint-disable-line
      controller: 'TransportCtrl as ctrl',
    })
    .otherwise({
      redirectTo: '/login',
      template: require('../views/login.html'),// eslint-disable-line
      controller: 'UserAuthCtrl as ctrl',
    });

  $locationProvider.html5Mode(false);
}
