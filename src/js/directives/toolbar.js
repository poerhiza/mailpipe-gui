/* @ngInject */

const template = require('../../views/toolbar.html');

export default function ToolbarDirective() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {},
    template,
    controller: 'ToolbarCtrl as ctrl',
  };
}
